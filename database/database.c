#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <stdbool.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <time.h>
#include <fcntl.h>
#include <signal.h>

#define PORT 8080
#define MIN_SIZE 128
#define MED_SIZE 256
#define MAX_SIZE 1024

char userInfo[MIN_SIZE];
char errMsg[MAX_SIZE];
char dbPath[MED_SIZE];

void writeLog(const char *username, const char *command)
{
    time_t t;
    struct tm *tm_info;
    char timestamp[MIN_SIZE];
    char logEntry[MED_SIZE];
    char logPath[MAX_SIZE];

    time(&t);
    tm_info = localtime(&t);
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", tm_info);

    snprintf(logEntry, sizeof(logEntry), "%s:%s:%s", timestamp, username, command);

    snprintf(logPath, sizeof(logPath), "/home/aveee/sisop/fp/database/databases/logs/log_file.log");

    FILE *logFile = fopen(logPath, "a");
    if (logFile == NULL)
    {
        perror("Error opening log file");
        return;
    }

    fprintf(logFile, "%s\n", logEntry);

    fclose(logFile);
}

const char *loginAs(const char *cmd, int sock)
{
    if (strstr(cmd, "root"))
    {
        snprintf(userInfo, MIN_SIZE, "root");
    }
    else
    {
        char username[MIN_SIZE];

        sscanf(cmd, "LOGIN %s", username);

        if (username != NULL)
        {
            snprintf(userInfo, MIN_SIZE, "%s", username);
        }
        else
        {
            char *errMsg = "Username not recognized";
            send(sock, errMsg, strlen(errMsg), 0);
        }
    }

    return userInfo;
}

void grantPermission(const char *cmd, int sock)
{
    char username[MIN_SIZE];
    char dbName[MIN_SIZE];
    char path[MAX_SIZE];

    sscanf(cmd, "GRANT PERMISSION %s INTO %s", dbName, username);

    snprintf(path, MAX_SIZE, "/home/aveee/sisop/fp/database/databases/%s/%s_permission.txt", dbName, dbName);

    if (strcmp(userInfo, "root") != 0)
    {
        char *errMsg = "User not root";
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    if (username == NULL)
    {
        char *errMsg = "Username not recognized";
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    if (dbName == NULL)
    {
        char *errMsg = "Database not recognized";
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    FILE *fp = fopen(path, "a");
    if (fp == NULL)
    {
        char *errMsg = "Error opening file";
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    fprintf(fp, "%s\n", username);

    fclose(fp);
}

void createUser(const char *cmd, int sock)
{
    char username[MIN_SIZE];
    char password[MIN_SIZE];
    char path[MAX_SIZE];

    sscanf(cmd, "CREATE USER %s IDENTIFIED BY %s", username, password);

    snprintf(path, MAX_SIZE, "/home/aveee/sisop/fp/database/databases/users/users.txt");

    if (strcmp(userInfo, "root") != 0)
    {
        char *errMsg = "User not root";
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    FILE *fp = fopen(path, "a");
    if (fp == NULL)
    {
        char *errMsg = "Error opening file";
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    fprintf(fp, "\n%s %s", username, password);

    fclose(fp);
}

int checkPermission(const char *dbName, int sock)
{
    char line[MED_SIZE];
    char path[MAX_SIZE];

    snprintf(path, MAX_SIZE, "/home/aveee/sisop/fp/database/databases/%s/%s_permission.txt", dbName, dbName);

    if (strcmp(userInfo, "root") != 0)
    {
        FILE *fp = fopen(path, "r");
        if (fp == NULL)
        {
            char *errMsg = "Couldn't find the file";
            send(sock, errMsg, strlen(errMsg), 0);
        }

        int found = 0;

        while (fgets(line, sizeof(line), fp))
        {
            if (strstr(line, userInfo) != NULL)
            {
                found = 1;
                break;
            }
        }

        fclose(fp);

        return found;
    }
}

void useDB(const char *cmd, int sock)
{
    char dbName[MIN_SIZE];
    sscanf(cmd, "USE %s", dbName);

    if (strcmp(userInfo, "root") == 0)
    {
        snprintf(dbPath, MAX_SIZE, "/home/aveee/sisop/fp/database/databases/%s", dbName);
        return;
    }

    if (checkPermission(dbName, sock))
    {
        printf("%s accessed the %s\n", userInfo, dbName);
        snprintf(dbPath, MAX_SIZE, "/home/aveee/sisop/fp/database/databases/%s", dbName);
        return;
    }
    else
    {
        snprintf(errMsg, MAX_SIZE, "%s can't access the %s", userInfo, dbName);
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }
}

void createDB(const char *cmd, int sock)
{
    char dbName[MIN_SIZE];
    char path[MED_SIZE];

    sscanf(cmd, "CREATE DATABASE %s", dbName);
    snprintf(path, MED_SIZE, "/home/aveee/sisop/fp/database/databases/%s", dbName);

    if (mkdir(path, 0777) == -1)
    {
        char *errMsg = "Error creating directory";
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    char permissionFileName[MAX_SIZE];
    snprintf(permissionFileName, MAX_SIZE, "%s/%s_permission.txt", path, dbName);

    FILE *permissionFile = fopen(permissionFileName, "w");
    if (permissionFile == NULL)
    {
        char *errMsg = "Error creating permission file";
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    fprintf(permissionFile, "%s", userInfo);
    fclose(permissionFile);
}

void createTable(const char *cmd, int sock)
{
    char tableName[MIN_SIZE];
    char columns[10][50];
    char types[10][20];

    sscanf(cmd, "CREATE TABLE %s (%[^)])", tableName, columns[0]);

    if (dbPath == NULL)
    {
        snprintf(errMsg, MED_SIZE, "Use database first");
        send(sock, errMsg, strlen(errMsg), 0);
    }

    char *parenStart = strchr(cmd, '(');
    char *parenEnd = strchr(cmd, ')');

    if (parenStart != NULL && parenEnd != NULL)
    {
        int colCount = 0;
        char *token = strtok(parenStart + 1, ",");

        while (token != NULL && token < parenEnd)
        {
            sscanf(token, "%s %s", columns[colCount], types[colCount]);
            token = strtok(NULL, ",;)");
            colCount++;
        }

        char fileName[MAX_SIZE];
        snprintf(fileName, MAX_SIZE, "%s/%s.txt", dbPath, tableName);

        FILE *tableFile = fopen(fileName, "w");
        if (tableFile == NULL)
        {
            char *errMsg = "Error creating table file";
            send(sock, errMsg, strlen(errMsg), 0);
            return;
        }

        for (int i = 0; i < colCount; i++)
        {
            fprintf(tableFile, "%s (%s)", columns[i], types[i]);
            if (i < colCount - 1)
            {
                fprintf(tableFile, " , ");
            }
        }

        fprintf(tableFile, "\n");
        fclose(tableFile);
    }
    else
    {
        char *errMsg = "Invalid command format";
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }
}

int deleteFolder(const char *path, int sock)
{
    DIR *d = opendir(path);
    size_t path_len = strlen(path);

    int error = 0;

    if (d)
    {
        struct dirent *p;

        while (!error && (p = readdir(d)))
        {
            if (strcmp(p->d_name, ".") != 0 && strcmp(p->d_name, "..") != 0)
            {
                size_t len = path_len + strlen(p->d_name) + 2;
                char *filepath = malloc(len);

                if (filepath)
                {
                    struct stat statbuf;
                    snprintf(filepath, len, "%s/%s", path, p->d_name);

                    if (!stat(filepath, &statbuf))
                    {
                        if (S_ISDIR(statbuf.st_mode))
                        {
                            error = deleteFolder(filepath, sock);
                        }
                        else
                        {
                            if (unlink(filepath) != 0)
                            {
                                char *errMsg = "Error deleting file";
                                send(sock, errMsg, strlen(errMsg), 0);
                                error = 1;
                            }
                        }
                    }

                    free(filepath);
                }
                else
                {
                    char *errMsg = "Memory allocation error";
                    send(sock, errMsg, strlen(errMsg), 0);
                    error = 1;
                }
            }
        }

        closedir(d);

        if (!error && rmdir(path) != 0)
        {
            char *errMsg = "Error deleting directory";
            send(sock, errMsg, strlen(errMsg), 0);
            error = 1;
        }
    }
    else
    {
        char *errMsg = "Error opening directory";
        send(sock, errMsg, strlen(errMsg), 0);
        error = 1;
    }

    return error;
}

void dropDB(const char *cmd, int sock)
{
    char dbName[MIN_SIZE];
    sscanf(cmd, "DROP DATABASE %s", dbName);

    char path[MAX_SIZE];
    snprintf(path, MAX_SIZE, "/home/aveee/sisop/fp/database/databases/%s", dbName);

    // if (strcmp(userInfo, "root") != 0)
    // {
    //
    // }

    if (deleteFolder(path, sock) == 0)
    {
        snprintf(errMsg, MED_SIZE, "Database %s not found", dbName);
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }
}

void dropTable(const char *cmd, int sock)
{
    char tableName[MIN_SIZE];

    sscanf(cmd, "DROP TABLE %s", tableName);

    char fileName[MAX_SIZE];
    snprintf(fileName, MAX_SIZE, "%s/%s.txt", dbPath, tableName);

    if (dbPath == NULL)
    {
        snprintf(errMsg, MED_SIZE, "Use database first");
        send(sock, errMsg, strlen(errMsg), 0);
    }

    if (remove(fileName) != 0)
    {
        snprintf(errMsg, MED_SIZE, "Table %s not found", tableName);
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }
}

void dropColumn(const char *cmd, int sock)
{
    char columnName[MIN_SIZE];
    char tableName[MIN_SIZE];

    sscanf(cmd, "DROP COLUMN %s FROM %s", columnName, tableName);

    char fileName[MAX_SIZE];
    snprintf(fileName, MAX_SIZE, "%s/%s.txt", dbPath, tableName);

    if (dbPath == NULL)
    {
        snprintf(errMsg, MED_SIZE, "Use database first");
        send(sock, errMsg, strlen(errMsg), 0);
    }

    FILE *tableFile = fopen(fileName, "r+");
    if (tableFile == NULL)
    {
        snprintf(errMsg, MED_SIZE, "Table %s not found", tableName);
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    FILE *tempFile = fopen("/tmp/temporary.txt", "w");
    if (tempFile == NULL)
    {
        char *errMsg = "Error creating temporary file";
        send(sock, errMsg, strlen(errMsg), 0);
        fclose(tableFile);
        return;
    }

    char line[MAX_SIZE];
    int found = 0;

    while (fgets(line, sizeof(line), tableFile))
    {
        char *token = strtok(line, ",");

        while (token != NULL)
        {
            if (strstr(token, columnName) == NULL)
            {
                fprintf(tempFile, "%s,", token);
            }
            token = strtok(NULL, ",");
        }

        fseek(tempFile, -1, SEEK_CUR);
        fputs("\n", tempFile);
    }

    fclose(tableFile);
    fclose(tempFile);

    remove(fileName);
    rename("/tmp/temporary.txt", fileName);
}

void insertInto(const char *cmd, int sock)
{
    char tableName[MIN_SIZE];
    char values[MAX_SIZE];

    sscanf(cmd, "INSERT INTO %s (%[^)])", tableName, values);

    char fileName[MAX_SIZE];
    snprintf(fileName, MAX_SIZE, "%s/%s.txt", dbPath, tableName);

    if (dbPath == NULL)
    {
        snprintf(errMsg, MED_SIZE, "Use database first");
        send(sock, errMsg, strlen(errMsg), 0);
    }

    FILE *tableFile = fopen(fileName, "a");
    if (tableFile == NULL)
    {
        snprintf(errMsg, MED_SIZE, "Table %s not found", tableName);
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    char *valueToken = strtok(values, ",");
    while (valueToken != NULL)
    {
        char value[MIN_SIZE];
        if (sscanf(valueToken, "%s", value) == 1)
        {
            fprintf(tableFile, "%s", value);

            valueToken = strtok(NULL, ",");
            if (valueToken != NULL)
            {
                fprintf(tableFile, " , ");
            }
        }
    }

    fprintf(tableFile, "\n");

    fclose(tableFile);
}

void update(const char *cmd, int sock)
{
    char tableName[MIN_SIZE];
    char columnName[MIN_SIZE];
    char value[MAX_SIZE];

    sscanf(cmd, "UPDATE %s SET %[^=]=%[^;];", tableName, columnName, value);

    char fileName[MAX_SIZE];
    snprintf(fileName, MAX_SIZE, "%s/%s.txt", dbPath, tableName);

    FILE *tableFile = fopen(fileName, "r+");
    if (tableFile == NULL)
    {
        snprintf(errMsg, MED_SIZE, "Table %s not found", tableName);
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    char firstLine[MAX_SIZE];
    if (fgets(firstLine, sizeof(firstLine), tableFile) == NULL)
    {
        snprintf(errMsg, MED_SIZE, "Error reading table %s", tableName);
        send(sock, errMsg, strlen(errMsg), 0);
        fclose(tableFile);
        return;
    }

    char columnNames[MAX_SIZE];
    strcpy(columnNames, firstLine);

    char *pos = strstr(columnNames, columnName);
    if (pos == NULL)
    {
        printf("Column %s not found in table %s\n", columnName, tableName);
        fclose(tableFile);
        return;
    }

    int colPosition = pos - columnNames;

    fseek(tableFile, strlen(firstLine), SEEK_SET);

    char line[MAX_SIZE];
    while (fgets(line, sizeof(line), tableFile))
    {
        char tempLine[MAX_SIZE];
        strcpy(tempLine, line);

        char *token = strtok(tempLine, ",");
        int count = 0;

        while (token != NULL)
        {
            if (count == colPosition)
            {
                fseek(tableFile, -strlen(line), SEEK_CUR);
                fprintf(tableFile, "%s,", value);
            }
            else
            {
                fprintf(tableFile, "%s,", token);
            }
            token = strtok(NULL, ",");
            count++;
        }

        fseek(tableFile, -1, SEEK_CUR);
        fputs("\n", tableFile);
    }

    fclose(tableFile);
}

void deleteFrom(const char *cmd, int sock)
{
    char tableName[MIN_SIZE];
    sscanf(cmd, "DELETE FROM %s", tableName);

    char fileName[MAX_SIZE];
    snprintf(fileName, MAX_SIZE, "%s/%s.txt", dbPath, tableName);

    FILE *tableFile = fopen(fileName, "r+");
    if (tableFile == NULL)
    {
        snprintf(errMsg, MED_SIZE, "Table %s not found", tableName);
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    char firstLine[MAX_SIZE];
    if (fgets(firstLine, sizeof(firstLine), tableFile) == NULL)
    {
        snprintf(errMsg, MED_SIZE, "Error reading table %s", tableName);
        send(sock, errMsg, strlen(errMsg), 0);
        fclose(tableFile);
        return;
    }

    fclose(tableFile);
    tableFile = fopen(fileName, "w");
    fprintf(tableFile, "%s", firstLine);
    fclose(tableFile);
}

int startServer()
{
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
    {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    return new_socket;
}

void receiveCmd(int new_socket)
{
    char buffer[MAX_SIZE] = {0};

    const char *loginCmd = "LOGIN";
    const char *createUserCmd = "CREATE USER";
    const char *useCmd = "USE";
    const char *grantCmd = "GRANT PERMISSION";
    const char *createDBCmd = "CREATE DATABASE";
    const char *createTableCmd = "CREATE TABLE";
    const char *dropDBCmd = "DROP DATABASE";
    const char *dropTableCmd = "DROP TABLE";
    const char *dropColumnCmd = "DROP COLUMN";
    const char *insertCmd = "INSERT INTO";
    const char *updateCmd = "UPDATE";
    const char *deleteCmd = "DELETE FROM";
    const char *selectCmd = "SELECT";

    while (true)
    {
        int valread = read(new_socket, buffer, MAX_SIZE);

        if (valread <= 0)
        {
            break;
        }

        buffer[valread - 1] = '\0';

        printf("Received command from client: %s\n", buffer);

        size_t pos = strcspn(buffer, ";");

        if (pos < strlen(buffer))
        {
            buffer[pos] = '\0';
        }

        if (strstr(buffer, loginCmd))
        {
            loginAs(buffer, new_socket);
        }
        else if (strstr(buffer, createUserCmd))
        {
            createUser(buffer, new_socket);
        }
        else if (strstr(buffer, useCmd))
        {
            useDB(buffer, new_socket);
        }
        else if (strstr(buffer, grantCmd))
        {
            grantPermission(buffer, new_socket);
        }
        else if (strstr(buffer, createDBCmd))
        {
            createDB(buffer, new_socket);
        }
        else if (strstr(buffer, createTableCmd))
        {
            createTable(buffer, new_socket);
        }
        else if (strstr(buffer, dropDBCmd))
        {
            dropDB(buffer, new_socket);
        }
        else if (strstr(buffer, dropTableCmd))
        {
            dropTable(buffer, new_socket);
        }
        else if (strstr(buffer, dropColumnCmd))
        {
            dropColumn(buffer, new_socket);
        }
        else if (strstr(buffer, insertCmd))
        {
            insertInto(buffer, new_socket);
        }
        else if (strstr(buffer, updateCmd))
        {
            update(buffer, new_socket);
        }
        else if (strstr(buffer, deleteCmd))
        {
            deleteFrom(buffer, new_socket);
        }
        else
        {
            snprintf(errMsg, MED_SIZE, "Command is not recognized");
            send(new_socket, errMsg, strlen(errMsg), 0);
        }

        writeLog(userInfo, buffer);

        memset(buffer, 0, MAX_SIZE);
    }
}

int main(int argc, char const *argv[])
{
    int new_socket = startServer();

    receiveCmd(new_socket);

    close(new_socket);

    return 0;
}