# sisop-praktikum-modul-4-2023-MH-IT19

| Nama Anggota                    | NRP        |
| ------------------------------- | ---------- |
| Michael Wayne                   | 5027221037 |
| Muhammad Harvian Dito Syahputra | 5027221039 |
| Imam Nurhadi                    | 5027221046 |

# A. Autentikasi

pada poin A, terdapat case agar program client dapat melakukan autentikasi user untuk user root dan melakukan create user. Dengan databasenya sendiri dapat menerima perintah tersebut dan menerima command create user.

untuk client.c

pertama proses autentikasi akan dicek pada fungsi

```c
char *performLogin(int argc, char const *argv[])
{
    char *loginAs = (char *)malloc(MAX_SIZE * sizeof(char));

    if (argc == 5 && strcmp(argv[1], "-u") == 0 && strcmp(argv[3], "-p") == 0)
    {
        const char *username = argv[2];
        const char *password = argv[4];

        if (!loginClient(username, password))
        {
            printf("Login gagal\n");
            return NULL;
        }

        snprintf(loginAs, MED_SIZE, "LOGIN %s\n", username);
    }
    else if (argc == 1)
    {
        int checkRoot = (geteuid() == 0);

        if (checkRoot)
        {
            printf("Login as root\n");
        }
        else
        {
            printf("Error login as root\n");
            return NULL;
        }

        snprintf(loginAs, MED_SIZE, "LOGIN root\n");
    }

    return loginAs;
}
```

Dan jika tidak ada maka akan ada pesan Login gagal, namun jika sesuai dengan kondisi

```c
  int checkRoot = (geteuid() == 0);

        if (checkRoot)
        {
            printf("Login as root\n");
        }
```

dan berhasil melewati autentikasi database.c pada fungsi

```c
const char *loginAs(const char *cmd, int sock)
{
    if (strstr(cmd, "root"))
    {
        snprintf(userInfo, MIN_SIZE, "root");
    }
    else
    {
        char username[MIN_SIZE];

        sscanf(cmd, "LOGIN %s", username);

        if (username != NULL)
        {
            snprintf(userInfo, MIN_SIZE, "%s", username);
        }
        else
        {
            char *errMsg = "Username not recognized";
            send(sock, errMsg, strlen(errMsg), 0);
        }
    }

    return userInfo;
}
```

maka user akan masuk sebagai Root dan dapat mengeksekusi database.c bagian fungsi

```c
void createUser(const char *cmd, int sock)
{
    char username[MIN_SIZE];
    char password[MIN_SIZE];
    char path[MAX_SIZE];

    sscanf(cmd, "CREATE USER %s IDENTIFIED BY %s", username, password);

    snprintf(path, MAX_SIZE, "/home/aveee/sisop/fp/database/databases/users/users.txt");

    if (strcmp(userInfo, "root") != 0)
    {
        char *errMsg = "User not root";
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    FILE *fp = fopen(path, "a");
    if (fp == NULL)
    {
        char *errMsg = "Error opening file";
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    fprintf(fp, "\n%s %s", username, password);

    fclose(fp);
}
```

dan menjalankan program dengan mengeksekusi
![A_CMD](https://drive.google.com/uc?id=10Lv_OKpgMXhsloEP6DMvynwohkH4jkXh)

dan akan muncul hasilnya sebagai berikut :
![A_Hasil](https://drive.google.com/uc?id=11seOBCunN1UnIgNdzwKvPM41yvL5CZVv)

Kombinasi username dan password user disimpan pada direktori yang di definisikan pada database.c

```c
snprintf(path, MAX_SIZE, "/home/aveee/sisop/fp/database/databases/users/users.txt");
fprintf(fp, "\n%s %s", username, password);
```

# B. Autorisasi

pada case ke-2 ini, Untuk dapat mengakses database yang dia punya, permission dilakukan dengan command USE DATABASE; pada fungsi database.c ini

```c
void useDB(const char *cmd, int sock)
{
    char dbName[MIN_SIZE];
    sscanf(cmd, "USE %s", dbName);

    if (strcmp(userInfo, "root") == 0)
    {
        snprintf(dbPath, MAX_SIZE, "/home/aveee/sisop/fp/database/databases/%s", dbName);
        return;
    }

    if (checkPermission(dbName, sock))
    {
        printf("%s accessed the %s\n", userInfo, dbName);
        snprintf(dbPath, MAX_SIZE, "/home/aveee/sisop/fp/database/databases/%s", dbName);
        return;
    }
    else
    {
        snprintf(errMsg, MAX_SIZE, "%s can't access the %s", userInfo, dbName);
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }
}
```

Yang bisa memberikan permission atas database untuk suatu user hanya root. permission ini akan dicek di fungsi

```c
int checkPermission(const char *dbName, int sock)
{
    char line[MED_SIZE];
    char path[MAX_SIZE];

    snprintf(path, MAX_SIZE, "/home/aveee/sisop/fp/database/databases/%s/%s_permission.txt", dbName, dbName);

    if (strcmp(userInfo, "root") != 0)
    {
        FILE *fp = fopen(path, "r");
        if (fp == NULL)
        {
            char *errMsg = "Couldn't find the file";
            send(sock, errMsg, strlen(errMsg), 0);
        }

        int found = 0;

        while (fgets(line, sizeof(line), fp))
        {
            if (strstr(line, userInfo) != NULL)
            {
                found = 1;
                break;
            }
        }

        fclose(fp);

        return found;
    }
}

```

jika memenuhi syarat bahwa user adalah root atau sudah terdaftar, maka dapat menjalankan fungsi void useDB(const char \*cmd, int sock) dan memiliki akses untuk

dan jika user merupakan root, maka dapat mengakses hak istemewa dengan menjalankan fungsi

```c
void grantPermission(const char *cmd, int sock)
{
    char username[MIN_SIZE];
    char dbName[MIN_SIZE];
    char path[MAX_SIZE];

    sscanf(cmd, "GRANT PERMISSION %s INTO %s", dbName, username);

    snprintf(path, MAX_SIZE, "/home/aveee/sisop/fp/database/databases/%s/%s_permission.txt", dbName, dbName);

    if (strcmp(userInfo, "root") != 0)
    {
        char *errMsg = "User not root";
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    if (username == NULL)
    {
        char *errMsg = "Username not recognized";
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    if (dbName == NULL)
    {
        char *errMsg = "Database not recognized";
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    FILE *fp = fopen(path, "a");
    if (fp == NULL)
    {
        char *errMsg = "Error opening file";
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    fprintf(fp, "%s\n", username);

    fclose(fp);
}
```

Untuk command Poin B ini, dijalanlankan dengan
![B_CMD](https://drive.google.com/uc?id=13ibbRGS2cJ9dRoYmijCk5cmDketgwkqC)

Untuk hasilnya, bisa dilihat pada gambar berikut:
![B_CMD](https://drive.google.com/uc?id=1Oo5q5aWgQxwbh-H4FN32pHrK2FJvLNjy)

# C. Data Definition Language

pada case ini, user diharuskan dapat melakukan put penamaan database, tabel, dan kolom hanya angka dan huruf.
Semua user bisa membuat database, otomatis user tersebut memiliki permission untuk database tersebut.

Jika user berhasil login dan mengakses Database, maka user dapat menajalankan fungsi

```c
void createDB(const char *cmd, int sock)
{
    char dbName[MIN_SIZE];
    char path[MED_SIZE];

    sscanf(cmd, "CREATE DATABASE %s", dbName);
    snprintf(path, MED_SIZE, "/home/aveee/sisop/fp/database/databases/%s", dbName);

    if (mkdir(path, 0777) == -1)
    {
        char *errMsg = "Error creating directory";
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    char permissionFileName[MAX_SIZE];
    snprintf(permissionFileName, MAX_SIZE, "%s/%s_permission.txt", path, dbName);

    FILE *permissionFile = fopen(permissionFileName, "w");
    if (permissionFile == NULL)
    {
        char *errMsg = "Error creating permission file";
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    fprintf(permissionFile, "%s", userInfo);
    fclose(permissionFile);
}
```

pada fungsi tersebut, user akan dicek lagi apakah memiliki permission pada

```c
char permissionFileName[MAX_SIZE];
    snprintf(permissionFileName, MAX_SIZE, "%s/%s_permission.txt", path, dbName);

    FILE *permissionFile = fopen(permissionFileName, "w");
    if (permissionFile == NULL)
    {
        char *errMsg = "Error creating permission file";
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }
```

dan jika memiliki permission, maka akan dieksekusi
` fprintf(permissionFile, "%s", userInfo);`

hal ini hampir sama dengan case CREATE TABBLE dan DROP DATABASE; untuk CREATE TABBLE akan masuk ke fungsi

```c
void createTable(const char *cmd, int sock)
{
    char tableName[MIN_SIZE];
    char columns[10][50];
    char types[10][20];

    sscanf(cmd, "CREATE TABLE %s (%[^)])", tableName, columns[0]);

    if (dbPath == NULL)
    {
        snprintf(errMsg, MED_SIZE, "Use database first");
        send(sock, errMsg, strlen(errMsg), 0);
    }

    char *parenStart = strchr(cmd, '(');
    char *parenEnd = strchr(cmd, ')');

    if (parenStart != NULL && parenEnd != NULL)
    {
        int colCount = 0;
        char *token = strtok(parenStart + 1, ",");

        while (token != NULL && token < parenEnd)
        {
            sscanf(token, "%s %s", columns[colCount], types[colCount]);
            token = strtok(NULL, ",;)");
            colCount++;
        }

        char fileName[MAX_SIZE];
        snprintf(fileName, MAX_SIZE, "%s/%s.txt", dbPath, tableName);

        FILE *tableFile = fopen(fileName, "w");
        if (tableFile == NULL)
        {
            char *errMsg = "Error creating table file";
            send(sock, errMsg, strlen(errMsg), 0);
            return;
        }

        for (int i = 0; i < colCount; i++)
        {
            fprintf(tableFile, "%s (%s)", columns[i], types[i]);
            if (i < colCount - 1)
            {
                fprintf(tableFile, " , ");
            }
        }

        fprintf(tableFile, "\n");
        fclose(tableFile);
    }
    else
    {
        char *errMsg = "Invalid command format";
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }
}
```

Deklarasi Variabel pada create tabble untuk keperluan dalam menyimpan strukturnya

```c
char tableName[MIN_SIZE]: Menyimpan nama tabel yang akan dibuat.
char columns[10][50]: Menyimpan nama kolom-kolom tabel.
char types[10][20]: Menyimpan tipe data dari masing-masing kolom.
```

akan dilakukan cek user apakah sudah menggunakan USE DATABASE; nya atau belum pada

```c
if (dbPath == NULL)
   {
       snprintf(errMsg, MED_SIZE, "Use database first");
       send(sock, errMsg, strlen(errMsg), 0);
   }
```

jika iya maka dapat membuat tabble baru sesuai direktori yang ditentukan pada createDB, selanjutnya untuk drop DATABASE

Drop database dengan melakukan pengecekan path folder apakah terdapat database

```c
void dropDB(const char *cmd, int sock)
{
    char dbName[MIN_SIZE];
    sscanf(cmd, "DROP DATABASE %s", dbName);

    char path[MAX_SIZE];
    snprintf(path, MAX_SIZE, "/home/aveee/sisop/fp/database/databases/%s", dbName);

    // if (strcmp(userInfo, "root") != 0)
    // {
    //
    // }

    if (deleteFolder(path, sock) == 0)
    {
        snprintf(errMsg, MED_SIZE, "Database %s not found", dbName);
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }
}

//Pada database, hampir sama dengan sebelunya, yaitu mencari kecocokan path database, nama File tabble, jika semua cocok maka baru bisa dieksekusi
void dropTable(const char *cmd, int sock)
{
    char tableName[MIN_SIZE];

    sscanf(cmd, "DROP TABLE %s", tableName);

    char fileName[MAX_SIZE];
    snprintf(fileName, MAX_SIZE, "%s/%s.txt", dbPath, tableName);

    if (dbPath == NULL)
    {
        snprintf(errMsg, MED_SIZE, "Use database first");
        send(sock, errMsg, strlen(errMsg), 0);
    }

    if (remove(fileName) != 0)
    {
        snprintf(errMsg, MED_SIZE, "Table %s not found", tableName);
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }
}

//pada drop column, juga sama, namun terdapat penambahan pembuatan temporary file untuk kebutuhan case kedepannya
void dropColumn(const char *cmd, int sock)
{
    char columnName[MIN_SIZE];
    char tableName[MIN_SIZE];

    sscanf(cmd, "DROP COLUMN %s FROM %s", columnName, tableName);

    char fileName[MAX_SIZE];
    snprintf(fileName, MAX_SIZE, "%s/%s.txt", dbPath, tableName);

    if (dbPath == NULL)
    {
        snprintf(errMsg, MED_SIZE, "Use database first");
        send(sock, errMsg, strlen(errMsg), 0);
    }

    FILE *tableFile = fopen(fileName, "r+");
    if (tableFile == NULL)
    {
        snprintf(errMsg, MED_SIZE, "Table %s not found", tableName);
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    FILE *tempFile = fopen("/tmp/temporary.txt", "w");
    if (tempFile == NULL)
    {
        char *errMsg = "Error creating temporary file";
        send(sock, errMsg, strlen(errMsg), 0);
        fclose(tableFile);
        return;
    }

    char line[MAX_SIZE];
    int found = 0;

    while (fgets(line, sizeof(line), tableFile))
    {
        char *token = strtok(line, ",");

        while (token != NULL)
        {
            if (strstr(token, columnName) == NULL)
            {
                fprintf(tempFile, "%s,", token);
            }
            token = strtok(NULL, ",");
        }

        fseek(tempFile, -1, SEEK_CUR);
        fputs("\n", tempFile);
    }

    fclose(tableFile);
    fclose(tempFile);

    remove(fileName);
    rename("/tmp/temporary.txt", fileName);
}
```

untuk dokumentasi sebagai berikut:
Bagian DDL_CREATE DB_hasil
![DDL_CREATE DB_hasil](https://drive.google.com/uc?id=1JjwWX4UXV1VCQsdmbLLzf3QyyiBEzZbX)
Bagian DDL_CREATE TABLE_cmd
![DDL_CREATE TABLE_cmd](https://drive.google.com/uc?id=1HWyjZEa9Qhs8lpW6_aHYO3qw7w2k88Za)
Bagian hasil DropDatabase
![DropDatabase](https://drive.google.com/uc?id=1c9gW6Y9VOodnRMOhfHwtAWuAVykCkwW8)
Bagian hasil DropTabble
![DropTabble](https://drive.google.com/uc?id=1qPcocmRCM3n7YDcXng1nun4kw8Y85N7U)
Bagian hasil DropColumn
![DropColumn](https://drive.google.com/uc?id=1iVUjriNUh_TpWEb22Q7uOlBeH6G5YLqO)
Bagian hasil Drop
![DropColumn](https://drive.google.com/uc?id=1rnyHpan84vntFMtr98MEnZHiQmh9HEN8)

# D. Data Manipulation Language

Pada soal urutan ke-D, praktikan diminta untuk membuat sebuah sistem DML yang terdiri dari INSERT, UPDATE, DELETE, SELECT, DAN WHERE.

1. INSERT

```c
void insertInto(const char *cmd, int sock)
{
    char tableName[MIN_SIZE];
    char values[MAX_SIZE];

    sscanf(cmd, "INSERT INTO %s (%[^)])", tableName, values);

    char fileName[MAX_SIZE];
    snprintf(fileName, MAX_SIZE, "%s/%s.txt", dbPath, tableName);

    if (dbPath == NULL)
    {
        snprintf(errMsg, MED_SIZE, "Use database first");
        send(sock, errMsg, strlen(errMsg), 0);
    }

    FILE *tableFile = fopen(fileName, "a");
    if (tableFile == NULL)
    {
        snprintf(errMsg, MED_SIZE, "Table %s not found", tableName);
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    char *valueToken = strtok(values, ",");
    while (valueToken != NULL)
    {
        char value[MIN_SIZE];
        if (sscanf(valueToken, "%s", value) == 1)
        {
            fprintf(tableFile, "%s", value);

            valueToken = strtok(NULL, ",");
            if (valueToken != NULL)
            {
                fprintf(tableFile, " , ");
            }
        }
    }

    fprintf(tableFile, "\n");

    fclose(tableFile);
}
```

Command Insert
![DML_INSERT_CMD](/images/dml/DML_INSERT_cmd.png)

Output Insert
![DML_INSERT_UPDATE](/images/dml/DML_INSERT_hasil.png)

2. UPDATE

```c
void update(const char *cmd, int sock)
{
    char tableName[MIN_SIZE];
    char columnName[MIN_SIZE];
    char value[MAX_SIZE];

    sscanf(cmd, "UPDATE %s SET %[^=]=%[^;];", tableName, columnName, value);

    char fileName[MAX_SIZE];
    snprintf(fileName, MAX_SIZE, "%s/%s.txt", dbPath, tableName);

    FILE *tableFile = fopen(fileName, "r+");
    if (tableFile == NULL)
    {
        snprintf(errMsg, MED_SIZE, "Table %s not found", tableName);
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    char firstLine[MAX_SIZE];
    if (fgets(firstLine, sizeof(firstLine), tableFile) == NULL)
    {
        snprintf(errMsg, MED_SIZE, "Error reading table %s", tableName);
        send(sock, errMsg, strlen(errMsg), 0);
        fclose(tableFile);
        return;
    }

    char columnNames[MAX_SIZE];
    strcpy(columnNames, firstLine);

    char *pos = strstr(columnNames, columnName);
    if (pos == NULL)
    {
        printf("Column %s not found in table %s\n", columnName, tableName);
        fclose(tableFile);
        return;
    }

    int colPosition = pos - columnNames;

    fseek(tableFile, strlen(firstLine), SEEK_SET);

    char line[MAX_SIZE];
    while (fgets(line, sizeof(line), tableFile))
    {
        char tempLine[MAX_SIZE];
        strcpy(tempLine, line);

        char *token = strtok(tempLine, ",");
        int count = 0;

        while (token != NULL)
        {
            if (count == colPosition)
            {
                fseek(tableFile, -strlen(line), SEEK_CUR);
                fprintf(tableFile, "%s,", value);
            }
            else
            {
                fprintf(tableFile, "%s,", token);
            }
            token = strtok(NULL, ",");
            count++;
        }

        fseek(tableFile, -1, SEEK_CUR);
        fputs("\n", tableFile);
    }

    fclose(tableFile);
}
```

Command Update
![DML_UPDATE_CMD](/images/dml/DML_UPDATE_cmd.png)

Output Update
![DML_UPDATE_HASIL](/images/dml/DML_UPDATE_hasil.png)

3. DELETE

```c
void deleteFrom(const char *cmd, int sock)
{
    char tableName[MIN_SIZE];
    sscanf(cmd, "DELETE FROM %s", tableName);

    char fileName[MAX_SIZE];
    snprintf(fileName, MAX_SIZE, "%s/%s.txt", dbPath, tableName);

    FILE *tableFile = fopen(fileName, "r+");
    if (tableFile == NULL)
    {
        snprintf(errMsg, MED_SIZE, "Table %s not found", tableName);
        send(sock, errMsg, strlen(errMsg), 0);
        return;
    }

    char firstLine[MAX_SIZE];
    if (fgets(firstLine, sizeof(firstLine), tableFile) == NULL)
    {
        snprintf(errMsg, MED_SIZE, "Error reading table %s", tableName);
        send(sock, errMsg, strlen(errMsg), 0);
        fclose(tableFile);
        return;
    }

    fclose(tableFile);
    tableFile = fopen(fileName, "w");
    fprintf(tableFile, "%s", firstLine);
    fclose(tableFile);
}
```

Command Delete
![DML_DELETE_CMD](/images/dml/DML_DELETE_cmd.png)

Output Delete
![DML_DELETE_HASIL](/images/dml/DML_DELETE_hasil.png)

# E. Logging

Pada Soal ke-E, praktikan diminta untuk membuat logging

```c
void writeLog(const char *username, const char *command)
{
    time_t t;
    struct tm *tm_info;
    char timestamp[MIN_SIZE];
    char logEntry[MED_SIZE];
    char logPath[MAX_SIZE];

    time(&t);
    tm_info = localtime(&t);
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", tm_info);

    snprintf(logEntry, sizeof(logEntry), "%s:%s:%s", timestamp, username, command);

    snprintf(logPath, sizeof(logPath), "/home/aveee/sisop/fp/database/databases/logs/log_file.log");

    FILE *logFile = fopen(logPath, "a");
    if (logFile == NULL)
    {
        perror("Error opening log file");
        return;
    }

    fprintf(logFile, "%s\n", logEntry);

    fclose(logFile);
}
```

![LOGGIN](/images/logging/LOGGING.png)

# F. Reliability

# G. Tambahan

# H. Error Handling

Pada soal ke-H, praktikan diminta untuk membuat error handling tanpa keluar dari program client

![ERROR HANDLING](/images/error_handling/ERROR%20HANDLING.png)
