#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <stdbool.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <arpa/inet.h>

#define PORT 8080
#define MIN_SIZE 128
#define MED_SIZE 256
#define MAX_SIZE 1024

bool loginClient(const char *username, const char *password)
{
    const char *path = "/home/aveee/sisop/fp/database/databases/users/users.txt";
    char line[MED_SIZE];

    FILE *fp = fopen(path, "r");
    if (fp == NULL)
    {
        printf("File tidak ditemukan\n");
        return false;
    }

    while (fgets(line, sizeof(line), fp))
    {
        char storedUsername[MIN_SIZE];
        char storedPassword[MIN_SIZE];

        sscanf(line, "%s %s", storedUsername, storedPassword);

        if (strcmp(username, storedUsername) == 0 && strcmp(password, storedPassword) == 0)
        {
            fclose(fp);
            printf("Login as %s\n", username);
            return true;
        }
    }

    fclose(fp);
    return false;
}

int createSocket()
{
    struct sockaddr_in address;
    int sock = 0;
    struct sockaddr_in serv_addr;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("Socket creation error\n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
    {
        printf("Invalid address/Address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("Connection Failed \n");
        return -1;
    }

    return sock;
}

void sendCmd(int sock)
{
    char cmd[MAX_SIZE];

    while (true)
    {
        fgets(cmd, MAX_SIZE, stdin);

        send(sock, cmd, strlen(cmd), 0);
    }
}

void receiveMsg(int sock)
{
    char buffer[MAX_SIZE] = {0};
    int valread;

    while (true)
    {
        valread = read(sock, buffer, MAX_SIZE - 1);
        if (valread <= 0)
        {
            printf("Server disconnected or no data received\n");
            break;
        }

        buffer[valread] = '\0';
        printf("%s\n", buffer);
        memset(buffer, 0, sizeof(buffer));
    }
}

char *performLogin(int argc, char const *argv[])
{
    char *loginAs = (char *)malloc(MAX_SIZE * sizeof(char));

    if (argc == 5 && strcmp(argv[1], "-u") == 0 && strcmp(argv[3], "-p") == 0)
    {
        const char *username = argv[2];
        const char *password = argv[4];

        if (!loginClient(username, password))
        {
            printf("Login gagal\n");
            return NULL;
        }

        snprintf(loginAs, MED_SIZE, "LOGIN %s\n", username);
    }
    else if (argc == 1)
    {
        int checkRoot = (geteuid() == 0);

        if (checkRoot)
        {
            printf("Login as root\n");
        }
        else
        {
            printf("Error login as root\n");
            return NULL;
        }

        snprintf(loginAs, MED_SIZE, "LOGIN root\n");
    }

    return loginAs;
}

int main(int argc, char const *argv[])
{
    char *loginAs = performLogin(argc, argv);

    if (loginAs == NULL)
    {
        return 1;
    }

    int sock = createSocket();
    if (sock == -1)
    {
        return -1;
    }

    send(sock, loginAs, strlen(loginAs), 0);

    pid_t pid = fork();
    if (pid == 0)
    {
        receiveMsg(sock);
    }
    else if (pid > 0)
    {
        sendCmd(sock);
    }
    else
    {
        perror("Fork failed\n");
        return -1;
    }

    close(sock);

    return 0;
}